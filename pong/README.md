# Pong

Created for BYU Chip Camp by Kevin Cuzner  
Modified June 2017 Benjamin James

This project is designed to be cross-platform and will build on both Windows and
Linux.


## Windows

Setup Instructions:

Install both of the following:

 * [CMake](http://www.cmake.org/files/v3.2/cmake-3.2.2-win32-x86.exe)
 * [CodeBlocks with MinGW](http://sourceforge.net/projects/codeblocks/files/Binaries/13.12/Windows/codeblocks-13.12mingw-setup.exe)

If you have previous versions of this software, it may not work properly.

Download the .zip file of the project: 

 * Click on the "Downloads" button on the left side navigation bar\
 * Click "Download repository"
 
Right click the .zip file and select "Extract all"  
Select C:\Users\\{username}\ as the install location  
Rename the folder to "C_Pong" after extraction finishes  

Build Instructions:

 1. Open cmake-gui
 2. Point the source path towards this project's directory (\pong folder)
 3. Point the build path towards this project's directory\build (\pong\build folder)
 4. Click configure. Select CodeBlocks - MinGW Makefiles.  Click Finish
 5. Click generate when configure has finished.
 6. Open Code::Blocks
 7. Navigate to this project's directory/build and open the .cbp file
 8. In the toolbar, click on 'build', 'select target', 'pong' (default is 'all')
 9. Make whatever changes you want to the source code in paddles.c
 10. When ready to run the game, click 'build', click the 'build-and-run' button (F9)


## Linux

You must have installed

 * GCC
 * CMake
 * SDL2
 * pkg-config
 
If you don't know how to install linux packages, see  
[How to install linux packages](https://www.linux.com/blog/how-install-software-linux-introduction)

Instructions:

Extract files to desired location

Change the install directory path:

 * open build/cmakefiles/CMakeDirectoryInformation.cmake
 * change "%USERPROFILE%\C_Pong\pong" to the path of your install folder
 * same thing with the next line, but add "/build" on the end

Open a terminal in the main directory and execute the following:

```
cd build
cmake ..
make
```

After making any changes to the source files, you only have to run 
```
make
```

again.  You can use any text editor to edit paddles.c

When you want to run the program, type

```
./build/pong
```
