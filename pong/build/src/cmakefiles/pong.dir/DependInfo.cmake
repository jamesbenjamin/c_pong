# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "J:/pong/src/assets.S" "J:/pong/build/src/CMakeFiles/pong.dir/assets.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")
set(CMAKE_DEPENDS_CHECK_C
  "J:/pong/src/main.c" "J:/pong/build/src/CMakeFiles/pong.dir/main.c.obj"
  "J:/pong/src/paddles.c" "J:/pong/build/src/CMakeFiles/pong.dir/paddles.c.obj"
  "J:/pong/src/pong.c" "J:/pong/build/src/CMakeFiles/pong.dir/pong.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__WIN32__"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/../include"
  "../src/../sdl-mingw/include/SDL2"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
